$(function(){
    //选项卡
    var count = 1;
    $($(".con")[0]).show();
    $($(".bottomdirection")[0]).show();
    $(".tab").click(function(){
        $(".con").hide();
        $(".bottomdirection").hide();
        var count=$(".tab").index(this);
        $($(".con")[count]).show();
        $($(".bottomdirection")[count]).show();
    });

    //价格明细
    $("span.det").click(function(){
        layer.open({
            type: 1,
            title:'价格明细',
            skin: 'layui-layer-rim', //加上边框
            area: ['550px', '750px'], //宽高
            content: '<div class="sendbox"> ' +
            '<div class="inputs"> <span class="send-con">车型:</span>小面包车</div>' +
            '<div class="inputs"> <span class="send-con">起步价格:</span><span>2.0</span>元 (3公里以内)</div>' +
            '<div class="inputs"> <span class="send-con">超公里价格:</span><span>15</span>元 (每公里)</div>' +
            '<div class="inputs"> <span class="send-con">延时价格:</span><span>5</span>元 (10分钟)</div>' +
            '<div class="inputs"> <span class="send-con">多票价格:</span><span>10</span>元 (一个卸货地免费、多个卸货地按票加)</div>' +
            '<div class="inputs"> <span class="send-con">装卸费用:</span><input type="number" name="fhr"><i style="width: auto">元</i></div>' +
            '<div class="inputs"> <span class="send-con">起步价格:</span><span>2.0</span>元 (3公里)</div>' +
            '<div class="inputs"> <span class="send-con">延时费用:</span><input type="number" name="fhr"><i style="width: auto">元</i></div>' +
            '<div class="inputs"> <span class="send-con">超公里价:</span><span>30</span>元 (2公里)</div>' +
            '<div class="inputs"> <span class="send-con">多票费用:</span><input type="number" name="fhr"><i style="width: auto">元</i></div>' +
            '<div class="inputs"> <span class="send-con">装卸费用:</span><input type="number" name="fhr"><i style="width: auto">元</i></div>' +
            '<div class="inputs"> <span class="send-con">合计金额:</span><input type="number" name="fhr"><i style="width: auto">元</i></div>' +
            '<div class="s-btn">确认添加</div>' +
            '<div class="clear"></div>' +
            '</div>'
        });
    })

    //收货地址的添加与删除
    $('.add-con').delegate('span.iconfont.green','click',function(){
        $(".add-con").append('<div class="c-box">' +
            '<div class="change-box">' +
            '<div class="inputs"><span>收货人地址:</span><input type="text" name="fhr"></div>' +
            '<div class="inputs half"><span>收货人:</span><input type="text" name="fhr"></div>' +
            '<div class="inputs half"><span>联系电话:</span><input type="text" name="fhr"></div>' +
            '</div> ');
    })

    $('.add-con').delegate('span.iconfont.red','click',function(){
        $(this).parent().parent().parent().children(":last-child").remove();
    })


})