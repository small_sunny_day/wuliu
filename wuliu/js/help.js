$(function(){
    $(".p-nav span").click(function(){
        $(this).parent().parent().find(".p-con").slideToggle().parent().siblings().find(".p-con").slideUp();
        $(this).parent().parent().find(".p-nav").parent().siblings().find(".p-nav").removeClass("hover");
    })
    $(".p-nav.reco span").click(function(){
        if($(this).hasClass('glyphicon glyphicon-menu-up')){
            $(this).removeClass('glyphicon glyphicon-menu-up').addClass('glyphicon glyphicon-menu-down');
            //收起
        }else{
            $(this).removeClass('glyphicon glyphicon-menu-down').addClass('glyphicon glyphicon-menu-up');
            //展开
        }
    })
    //左侧与右侧内容对应
    var count = 1;
    $($(".per-right")[0]).show();
    $(".p-con p").click(function(){
        $(".per-right").hide();
        var count = $(".p-con p").index(this);
        console.log(count)
        $($(".per-right")[count]).show();
        $(".p-con p").addClass("left-bef");
        $(".p-con p").removeClass("left-aft");
        $(this).removeClass("left-bef");
        $(this).addClass("left-aft");
    })

})