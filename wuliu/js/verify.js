$(function(){
    // 这里使用最原始的js语法实现，可对应换成jquery语法进行逻辑控制
    var visible=document.getElementById('psw_visible');//text block
    var invisible=document.getElementById('psw_invisible');//password block
    var inputVisible = document.getElementById('input_visible');
    var inputInVisible = document.getElementById('input_invisible');
    var visible1=document.getElementById('psw_visible1');//text block
    var invisible1=document.getElementById('psw_invisible1');//password block
    var inputVisible1 = document.getElementById('input_visible1');
    var inputInVisible1 = document.getElementById('input_invisible1');
    //隐藏text block，显示password block
    $("#invisible").click(function(){
        var val=inputVisible.value;//将text的值传给password
        inputInVisible.value = val;
        invisible.style.display = "";
        visible.style.display = "none";
    })
    $("#visible").click(function(){
        var val = inputInVisible.value;//将password的值传给text
        inputVisible.value = val;
        invisible.style.display = "none";
        visible.style.display = "";
    })

    $("#invisible1").click(function(){
        var val=inputVisible1.value;//将text的值传给password
        inputInVisible1.value = val;
        invisible1.style.display = "";
        visible1.style.display = "none";
    })
    $("#visible1").click(function(){
        var val = inputInVisible1.value;//将password的值传给text
        inputVisible1.value = val;
        invisible1.style.display = "none";
        visible1.style.display = "";
    })
    // 判断两次密码的值
    var val = $("#input_invisible").val();
    var nval = $("#input_invisible1").val();
    if( val != nval){
        layer.msg("两次输入的密码不一致,请重新填写")
    }

})