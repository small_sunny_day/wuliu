$(function(){
    //我要发货
    $("#send").click(function(){
        layer.open({
            type: 1,
            title:'我要发货',
            skin: 'layui-layer-rim', //加上边框
            area: ['500px', '440px'], //宽高
            content: '<div class="sendbox">' +
            '<div class="box"><div class="icon"><span class="iconfont">&#xe631;</span></div><p>快速发货</p></div>' +
            '<div class="box"><div class="icon"><span  class="iconfont">&#xe631;</span></div><p>加急专车</p></div>' +
            '<div class="box"><div class="icon"><span  class="iconfont">&#xe70f;</span></div><p>仓储配送</p></div>' +
            '<div class="box"><div class="icon"><span  class="iconfont">&#xe804;</span></div><p>冷链物流</p></div>' +
            '<div class="clear"></div></div>'
        });
    })

    //返回顶部
    $(window).scroll(function(){
        if($(window).scrollTop()>100){
            $(".top").show();
        }else{
            $(".top").hide();
        }
        $(".top").click(function(){
            $({top: $(window).scrollTop()}).animate(
                {top: 0},
                {
                    duration: 500,
                    step: function() {
                        $(window).scrollTop(this.top);
                    }
                }
            );
        });
    });

    // 登录
    $("p.lo.lo-be").click(function(){
        window.location.href = "login.html";
    })
})

