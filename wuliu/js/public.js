$(function(){
    //返回顶部
    $(window).scroll(function(){
        if($(window).scrollTop()>50){
            $(".top").show();
        }else{
            $(".top").hide();
        }
        $(".top").click(function(){
            $({top: $(window).scrollTop()}).animate(
                {top: 0},
                {
                    duration: 500,
                    step: function() {
                        $(window).scrollTop(this.top);
                    }
                }
            );
        });
    });

    // 订单详情页面返回操作
    $(".tr-title").click(function(){
        window.location.href = "personal.html";
    })

})

