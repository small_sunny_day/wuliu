
/**
 * Created by Administrator on 2016/6/20.
 */

/*表格*/

var ghtable={
		//title,parent--父元素thead,courses--里面的字段--类型:数组,issort--是否排序,zdm--表头内字段
		title:function(obj){
			var parent=obj.parent,
			courses=obj.courses,
			issort=obj.issort||false,
			isfx=obj.isfx||false,
			zdm=obj.zdm,
			width=obj.width||[];
			$(parent).append('<tr>');
			var tmps='';
			if(isfx){
				tmps='<td class="fx"></td>'
			}
			if(issort){
				for(var i=0;i<courses.length;i++){
					tmps+=' <td name="'+zdm[i]+'" style="width:'+width[i]+'">'+courses[i]+'<i class="sort desc  glyphicon-triangle-top"></i><i class="sort asc  glyphicon-triangle-bottom"></i></td>'
				}
			}else{
				for(var i=0;i<courses.length;i++){
					tmps+=' <td name="'+zdm[i]+'" style="width:'+width[i]+'">'+courses[i]+'</td>'
				}
			}
			$(parent+' tr').append(tmps);
			return this;
		},
		//tbody parent--父元素,data--数据--[{}],special--特殊的--[{name:name,c:class},...],sort--排序方式默认为正序,page--当前页,iffx--是否需要复选框--ture和false
		tbody:function(obj){
			var parent=obj.parent,
			data=obj.data,
			special=obj.special||[],
			page=obj.page,
			titlezi=obj.titlezi,
			isfx=obj.isfx||false;
			//sort=sort||'asc';
			var tmps1='';
			for(var k=0;k<data.length;k++){
				if(isfx){
					tmps1='<td class="fx" name="'+data[k].id+'"><input type="checkbox"></td>';
				}
				for(var i=0;i<titlezi.length;i++){	
					if(data[k][titlezi[i]]||data[k][titlezi[i]]=='0'){
						tmps1 +='<td name="'+titlezi[i]+'">'+data[k][titlezi[i]]+'</td>'
					}else{
						tmps1 +='<td name="'+titlezi[i]+'">--</td>'
					}

				}
				$(parent).append('<tr>');
				$($(parent+' tr')[k]).append(tmps1);
				$($(parent+' tr')[k]).attr('name',data[k].id);
				tmps1='';
			}

			$(parent+' tr').each(function(){
				if($(this).prev()){
					if($(this).prev().hasClass('white')){
						$(this).addClass('ccc')
					}else{
						$(this).addClass('white')
					}
				}else{
					$(this).addClass('white')
				}
			})
			$('tr').parent().parent().attr('page',''+page);
			//特殊列处理
			for(var j=0;j<special.length;j++){
				$(parent+' td[name='+special[j].name+']').attr('class',''+special[j].c);	
			};
			return this;
		},
		//分页,parent-分页所属表的父元素,url-ajax访问地址,pageNum---分页请求ajax时传给后台的变量,datas---ajax时需要传回后台的数据--类型为对象类型
		pages:function(obj){
			var parent=obj.parent,
			pages=obj.pages,
			url=obj.url,
			callback=obj.callback,
//			pageNum=obj.pageNum||'pageNum',
			datas=obj.data||{};
			orderIds=obj.orderIds||[];
			$(parent+' .pagination').children().remove();
			var tmps='<li><a  aria-label="Previous" page="pre"><span aria-hidden="true">&laquo;</span></a></li>';

			for(var i=1;i<pages+1;i++){
				tmps+='<li><a page="'+i+'">'+i+'</a></li>';
			}
			tmps+='<li><a  aria-label="Next" page="next"><span aria-hidden="true">&raquo;</span></a></li>'
				$(parent+' .pagination').append(tmps)
				$(parent+' .pagination a').click(function(){
					//某些功能特有的 下个项目可删除
					$('.tccdd tbody input:checked').each(function(){
						orderIds.push($(this).parents('tr').attr('name'));
					})
					var page=0;
					$(parent+' .pagination a').removeClass('xz');
					$(this).addClass('xz');
					if($(this).attr('page')=='pre'){
						page=Number($(parent+' table').attr('page'))-1;
						console.log(page)
						if(page==0){
							if(pages - 1 == 0){
								return;
							}else{
								layer.msg('当前页是首页！');
								return;
								//page=pages;
							}
						}
					}else if($(this).attr('page')=='next'){
						page=Number($(parent+' table').attr('page'))+1;
						console.log(page)
						if(page==pages+1){

							if(pages + 1 == 2){
								return;
							}else{
								layer.msg('当前页是尾页！');
								return;
								// page=1;
							}
						}
					}else {
						page=Number($(this).attr('page'));
						console.log(page)
					}
					datas['pageNum']=page;
					$.ajax({
						"dataType" : 'json',
						"type" : "POST",
						"url" : url,
						"cache" : false,
						"data":{pageNumber:page},
						"success" : function(data) {
							data.listRouteId=datas.routeid;
							data.search=datas;
							console.log(data);
							callback(data);
						}
					}); 
				})
				return this;
		},
		pagesqt:function(obj){
			var parent=obj.parent,
			pages=obj.pages,
			url=obj.url,
			callback=obj.callback,
//			pageNum=obj.pageNum||'pageNum',
			datas=obj.data||{},
			select=obj.select||'table';
			$(parent+' .pagination').children().remove();
			var tmps='<li><a  aria-label="Previous" page="pre"><span aria-hidden="true">&laquo;</span></a></li>';

			for(var i=1;i<pages+1;i++){
				tmps+='<li><a page="'+i+'">'+i+'</a></li>';
			}
			tmps+='<li><a  aria-label="Next" page="next"><span aria-hidden="true">&raquo;</span></a></li>'
				$(parent+' .pagination').append(tmps)
				$(parent+' .pagination a').click(function(){
					var page=0;
					$(parent+' .pagination a').removeClass('xz');
					$(this).addClass('xz');
					if($(this).attr('page')=='pre'){
						page=Number($(parent+' table').attr('page'))-1;
						console.log(page)
						if(page==0){
							if(pages - 1 == 0){
								return;
							}else{
								layer.msg('当前页是首页！');
								return;
								//page=pages;
							}
						}
					}else if($(this).attr('page')=='next'){
						page=Number($(parent+' table').attr('page'))+1;
						console.log(page)
						if(page==pages+1){

							if(pages + 1 == 2){
								return;
							}else{
								layer.msg('当前页是尾页！');
								return;
								// page=1;
							}
						}
					}else {
						page=Number($(this).attr('page'));
						console.log(page)
					}
					console.log(page);
					datas['pageNum']=page;
					$.ajax({
						"dataType" : 'json',
						"type" : "POST",
						"url" : url,
						"cache" : false,
						"data":{pageNumber:page},
						"success" : function(data) {
							console.log(data);
							callback(data);
						}
					}); 
				})
				return this;
		},
		//input,spantext--数组,type--数组
		input:function(obj){
			var parent=obj.parent,
			spantext=obj.spantext,
			type=obj.type,
			names=obj.names,
			values=obj.values||'null',
			lasttext=obj.lasttext||[];
			var tmps='';
			if(values=='null'){
				if(lasttext.length!=0){
					for(var i=0;i<spantext.length;i++){
						tmps +='<div class="inputs" ><span>'+spantext[i]+'</span><input type="'+type[i]+'" name="'+names[i]+'"><i style="width: auto">'+lasttext[i]+'</i></div>';
					}
				}else{
					for(var i=0;i<spantext.length;i++){
						tmps +='<div class="inputs" ><span>'+spantext[i]+'</span><input type="'+type[i]+'" name="'+names[i]+'"></div>';
					}
				}
			}else{
				if(lasttext.length!=0){
					for(var i=0;i<spantext.length;i++){
						tmps +='<div class="inputs"><span>'+spantext[i]+'</span><input name="'+names[i]+'" type="'+type[i]+'" value="'+values[i]+'"><i style="width: auto">'+lasttext[i]+'</i></div>';
					}
				}else{
					for(var i=0;i<spantext.length;i++){
						tmps +='<div class="inputs"><span>'+spantext[i]+'</span><input name="'+names[i]+'" type="'+type[i]+'" value="'+values[i]+'"></div>';
					}
				}
			}
			$(parent).append(tmps);
			$(parent+' input').each(function(){
				$(this).change(function(){

				})
			})
			return this;
		},
		//下拉:parent,option--[[{value:1,text:1},{value:1,text:1}],[{value:1,text:1},{value:1,text:1}]]
		select:function(obj) {
			var parent=obj.parent,
			spantext=obj.spantext,
			names=obj.names,
			option=obj.option||[];
			var tmps = '';
			for (var i = 0; i < spantext.length; i++) {
				tmps += '<div class="inputs" ><span>' + spantext[i] + '</span><select name="' + names[i] + '" id=""></select></div>';
			}
			$(parent).append(tmps);
			for (var j = 0; j < names.length; j++) {
				for (var l = 0; l < option[j].length; l++) {
					$(parent + ' select[name=' + names[j] + ']').append('<option value="' + option[j][l].value + '">' + option[j][l].text + '</option>');
				}
			}
			return this;
		},
		//单选按钮 values----[{}]--radio的value和text
		radio:function(obj){
			var parent=obj.parent,
			spantext=obj.spantext,
			names=obj.names,
			values=obj.values;

			var tmps = '',tmpss='';
			for (var i = 0; i < spantext.length; i++) {
				tmps += '<div class="inputs radios" ><span>' + spantext[i] + '</span></div>';
				$(parent).append(tmps);
				tmps='';
				//tmps += '<div class="inputs" ><span>' + spantext[i] + '</span><input type="radio" value="'+value[i][j].value+'" name="'+names[i]+'">'+value[i][j].text+'</div>';
				for (var j = 0; j< values[i].length; j++) {
					if(j==0){
						tmpss+='<input type="radio" value="'+values[i][j].value+'" name="'+names[i]+'" checked><b>'+values[i][j].text+'</b>'
					}else{
						tmpss+='<input type="radio" value="'+values[i][j].value+'" name="'+names[i]+'"><b>'+values[i][j].text+'</b>'
					}
				}
				$($(parent+' .radios')[i]).append(tmpss);
				tmpss='';
			}
			return this;
		},
		sousuo:function(obj){
			var parent=obj.parent,
			value=obj.value,
			url=obj.url,
			search=obj.data||{},
			keywords=obj.keywords||'keywords',
			callback=obj.callback;
			var tmps='<input type="text" placeholder="'+value+'"><div class="ss glyphicon glyphicon-search"></div>';
			$(parent).append(tmps);
			$(parent+' .ss').click(function(){
				var val=$(parent+' input').val().trim();
				search[keywords]=val;
				$.ajax({
					"dataType" : 'json',
					"type" : "POST",
					"url" : url,
					"cache" : false,
					"data":search,
					"success" : function(data) {
						console.log(data);
						data.search=search;
						callback(data);
					}
				});
			})
			return this;
		},
		//自定义树形
		shu:function(parent,data){
			var tmps='';var tmpsc='',t=[];
			for(var i=0;i<data.length;i++){	
				if(data[i].children.length!=0){
					tmps+='<li id="'+data[i].id+'"class="hasc"><input type="checkbox"><span class="qx-sel-name">'+data[i].text+'</span></li><ul class="qx-child"></ul>';
					$(parent).append(tmps);
					t=data[i].children;
					for(var j=0;j<t.length;j++){
						tmpsc+='<li class="qx-c" id="'+t[j].id+'"><input type="checkbox">'+t[j].text+'</li>';
					}
					$($('.qx-child')[$('.qx-child').length-1]).append(tmpsc);
					tmpsc='';
				}else{
					tmps+='<li id="'+data[i].id+'" class="noc"><input type="checkbox"><span class="qx-sel-name">'+data[i].text+'</span></li>';
					$(parent).append(tmps);
				}
				tmps='';
			}
			$('.hasc .qx-sel-name').click(function(){
				var index=$('.hasc .qx-sel-name').index(this);
				console.log(index);
				$($('.qx-child')[index]).finish();
				$($('.qx-child')[index]).slideToggle();
			})
			$('.hasc input[type=checkbox]').click(function(){
				var index=$('.hasc input[type=checkbox]').index(this);
				//    		$($('.qx-child')[index]) 		
				if($(this).prop("checked")){
					$($('.qx-child')[index]).find('input').each(function(){
						$(this).prop('checked',true)
					})
				}else{
					$($('.qx-child')[index]).find('input').each(function(){
						$(this).prop('checked',false)
					})
				}
			})
			$('.qx-c input[type=checkbox]').click(function(){
				$(this).parent().parent().prev().find('input').prop('checked',true);
			})
			return this;
		},
		//判断选择时间和当前时间差是否差60
		sjc:function(curr,xzsj){
			if(ghtable.DateDiff(curr,xzsj)>60){
				return false;
			}else{
				return true;
			}
			return this;
		},
		//计算天数差的函数，通用  
		DateDiff:function(sDate1,  sDate2){    //sDate1和sDate2是2006-12-18格式  
			var  aDate,  oDate1,  oDate2,  iDays  
			aDate  =  sDate1.split("-")  
			oDate1  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])    //转换为12-18-2006格式  
			aDate  =  sDate2.split("-")  
			oDate2  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])  
			iDays  =  parseInt(Math.abs(oDate1  -  oDate2)  /  1000  /  60  /  60  /24)    //把相差的毫秒数转换为天数  
			return  iDays   
		},
}
Array.prototype.unique4 = function()
{
	this.sort();
	var re=[this[0]];
	for(var i = 1; i < this.length; i++)
	{
		if( this[i] !== re[re.length-1])
		{
			re.push(this[i]);
		}
	}
	return re;
}
localStorage.ghtable=JSON.stringify(ghtable);
for(var i in ghtable){
	localStorage['ghtable_'+i]=ghtable[i]
}
var table=eval(localStorage.ghtable);
