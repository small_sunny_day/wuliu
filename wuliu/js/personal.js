$(function(){
    $(".p-nav span").click(function(){
        $(this).parent().parent().find(".p-con").slideToggle().parent().siblings().find(".p-con").slideUp();
        $(this).parent().parent().find(".p-nav").parent().siblings().find(".p-nav").removeClass("hover");
    })
    $(".p-nav.reco span").click(function(){
        if($(this).hasClass('glyphicon glyphicon-menu-up')){
            $(this).removeClass('glyphicon glyphicon-menu-up').addClass('glyphicon glyphicon-menu-down');
            //收起
        }else{
            $(this).removeClass('glyphicon glyphicon-menu-down').addClass('glyphicon glyphicon-menu-up');
            //展开
        }
    })
    //左侧与右侧内容对应
    var count = 1;
    $($(".per-right")[0]).show();
    $(".p-con p").click(function(){
        $(".per-right").hide();
        var count = $(".p-con p").index(this);
        console.log(count)
        $($(".per-right")[count]).show();
        $(".p-con p").addClass("left-bef");
        $(".p-con p").removeClass("left-aft");
        $(this).removeClass("left-bef");
        $(this).addClass("left-aft");
    })
    // 我的车库
    // 公司车辆与我的熟车\
    var count1 = 1;
    $($(".car-tab")[0]).show();
    $($(".car-right")[0]).show();
    $(".ctab").click(function(){
        $(".car-tab").hide();
        $(".car-right").hide();
        var count1 = $(".ctab").index(this);
        $($(".car-tab")[count1]).show();
        $($(".car-right")[count1]).show();
        $(".ctab").addClass("black");
        $(".ctab").removeClass("org");
        $(this).removeClass("black");
        $(this).addClass("org");
    });
    // 我的车库添加车辆
    $("#c-add").click(function(){
        layer.open({
            type: 1,
            title:'添加车辆',
            skin: 'layui-layer-rim', //加上边框
            area: ['550px', '410px'], //宽高
            content: '<div class="sendbox"> ' +
            '<div class="group"> <span>司机姓名:</span><input type="number" name="fhr"></div>' +
            '<div class="group"> <span>联系电话:</span><input type="number" name="fhr"></div>' +
            '<div class="group"> <span>车型:</span><input type="number" name="fhr"></div>' +
            '<div class="group"> <span>车牌号码:</span><input type="number" name="fhr"></div>' +
            '<div class="s-btn">确认添加</div>' +
            '<div class="clear"></div>' +
            '</div>'
        });
    })
    // 我的熟车添加车辆
    $("#s-add").click(function(){
        layer.open({
            type: 1,
            title:'添加车辆',
            skin: 'layui-layer-rim', //加上边框
            area: ['550px', '480px'], //宽高
            content: '<div class="sendbox"> ' +
            '<div class="group"> <span>司机姓名:</span><input type="text" name="fhr"></div>' +
            '<div class="group"> <span>联系电话:</span><input type="text" name="fhr"></div>' +
            '<div class="group"> <span>车型:</span><select name="" id=""><option value="">小面包车</option><option value="">大面包车</option><option value="">小货车</option></select></div>' +
            '<div class="group"> <span>车牌号码:</span><input type="text" name="fhr"></div>' +
            '<div class="group h-inp"> <span>长跑路线:</span><input type="text" name="fhr"><span class="h-fg">-</span><input type="number" name="fhr"></div>' +
            '<div class="group h-inp"> <span>附加路线:</span><input type="text" name="fhr"><span class="h-fg">-</span><input type="number" name="fhr"></div>' +
            '<div class="s-btn">确认添加</div>' +
            '<div class="clear"></div>' +
            '</div>'
        });
    })
    // 物流公司
    // 添加公司
    $("#com-add").click(function(){
        layer.open({
            type: 1,
            title:'添加车辆',
            skin: 'layui-layer-rim', //加上边框
            area: ['550px', '220px'], //宽高
            content: '<div class="sendbox"> ' +
            '<div class="group"> <span>物流公司名称:</span><input type="text" name="fhr"></div>' +
            '<div class="s-btn">确认添加</div>' +
            '<div class="clear"></div>' +
            '</div>'
        });
    })
    // 我的线路
    // 添加线路
    $("#rou-add").click(function(){
        layer.open({
            type: 1,
            title:'添加线路',
            skin: 'layui-layer-rim', //加上边框
            area: ['550px', '220px'], //宽高
            content: '<div class="sendbox"> ' +
            '<div class="group h-inp"> <span>长跑路线:</span><input type="text" name="fhr"><span class="h-fg">-</span><input type="text" name="fhr"></div>' +
            '<div class="s-btn">确认添加</div>' +
            '<div class="clear"></div>' +
            '</div>'
        });
    })

    // 我的钱包
    // 可提现余额
    var bala = $(".balan").text();
    $(".ance").text(bala)

    // 订单查询
    // 订单查询
    var count2 = 1;
    $($(".order-tab")[0]).show();
    $($(".order-right")[0]).show();
    $(".otab").click(function(){
        $(".order-tab").hide();
        $(".order-right").hide();
        var count2 = $(".otab").index(this);
        $($(".order-tab")[count2]).show();
        $($(".order-right")[count2]).show();
        $(".otab").addClass("black");
        $(".otab").removeClass("org");
        $(this).removeClass("black");
        $(this).addClass("org");
    });
     // 查看位置弹窗
     $(".address").click(function(){
         layer.open({
             type: 1,
             title:'查看位置',
             skin: 'layui-layer-rim', //加上边框
             area: ['550px', '600px'], //宽高
             content: '<div class="map">' +
             '	<div id="allmap"></div>' +
             '</div>'
         });
     })

    // 我的钱包(银行卡号用*代替)
     var str = $(".cnum").html();
     str = str.replace(/[&nbsp;]+/g,'');
     var a1 = str.substring(0,4);
     var a = a1.replace(a1,"****");
     var b1 = str.substring(4,8);
     var b = b1.replace(b1,"****");
     var c1 = str.substring(8,12);
     var c = c1.replace(c1,"****");
     var d1 = str.substring(12,16);
     var d = d1.replace(d1,"****")
     var e = str.substring(16,19);
     var nht = a+'&nbsp;'+b+'&nbsp;'+c+'&nbsp;'+d+'&nbsp;'+e;
     $(".cnum").html(nht)

})